import express from "express";
import { dirname, join } from "path";
import { fileURLToPath } from "url";

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
const port = process.env.PORT;

const app = express();

app.use(express.json());

// Static folder
app.use(express.static(join(__dirname, "../public")));

app.get("", (req, res) => {
  res.render("index");
});

app.listen(port, () => console.log(`Server started on port: ${port}`));
